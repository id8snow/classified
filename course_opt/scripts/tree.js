var _=require('lodash');

var colleges=require('./data/college-list.json').colleges;
var degrees=require('./data/degrees.json');
var alias=require('./data/course-alias.json');
var courses=require('./data/course-schedule-f15.json').courses;
var courses2=require('./data/course-schedule-s16.json').courses;


var by_req={};
var course_lookup={};

_.map(courses,function(d,k){
	course_lookup[k]=d.name.toLowerCase();
	course_lookup[d.name.toLowerCase()]=k;
});

_.map(courses2,function(d,k){
	course_lookup[k]=d.name.toLowerCase();
	course_lookup[d.name.toLowerCase()]=k;
});

_.map(alias,function(ar,key){
	_.map(ar,function(name){
		course_lookup[name]=key;
	});
});

console.log(course_lookup);

var degree_name,req;

_.map(colleges,function(c){
	_.map(degrees[c],function(v){
		degree_name=v.name.toLowerCase();

		by_req[degree_name]=[];

		_.map(v.requirements,function(k){
			req=k.toLowerCase();

			if(course_lookup[req]){
				by_req[degree_name].push(course_lookup[req]);
			}else{
				by_req[degree_name].push(req);
			}
		});
	});
});

/*
Files look slightly different when moved, so if it's missing something try below
*/

// var _=require('lodash');

// var colleges=require('./data/college-list.json').colleges;
// var degrees=require('./data/degrees.json');
// var alias=require('./data/course-alias.json');
// var courses=require('./data/course-schedule-f15.json').courses;
// var courses2=require('./data/course-schedule-s16.json').courses;


// var by_req={};
// var course_lookup={};
// var course_reqs={};

// _.map(courses,function(d,k){
// 	course_lookup[k]=d.name.toLowerCase();
// 	course_lookup[d.name.toLowerCase()]=k;
// });
// course_reqs=_.mapValues(courses,'prereqs');
// _.map(courses2,function(d,k){
// 	course_lookup[k]=d.name.toLowerCase();
// 	course_lookup[d.name.toLowerCase()]=k;
// });
// _.merge(course_reqs,_.mapValues(courses2,'prereqs'));

// _.map(alias,function(ar,key){
// 	_.map(ar,function(name){
// 		course_lookup[name]=key;
// 	});
// });

// console.log(course_lookup);

// var degree_name,req;

// _.map(colleges,function(c){
// 	_.map(degrees[c],function(v){
// 		degree_name=v.name.toLowerCase();

// 		by_req[degree_name]=[];

// 		_.map(v.requirements,function(k){
// 			req=k.toLowerCase();

// 			if(course_lookup[req]){
// 				by_req[degree_name].push(course_lookup[req]);
// 			}else{
// 				by_req[degree_name].push(req);
// 			}
// 		});
// 	});
// });

// console.log(by_req);
// _.map(degrees,function(d,k){
// 	console.log(d);
// })