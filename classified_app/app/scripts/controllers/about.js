'use strict';

/**
 * @ngdoc function
 * @name classifiedApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the classifiedApp
 */
angular.module('classifiedApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
